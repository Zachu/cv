---
title: Curriculum vitae
date: 2024-05-20
author: Jani "Zachu" Korhonen
---

## Basic information

```yaml
Full name: Jani Petri Juhani Korhonen
Email:     jani@zachu.fi
Phone:     +358 (0)50 413 8763
Born:      1987-03-13
```

## Work experience

### Lead DevOps Engineer at [Supermetrics](https://www.supermetrics.com/) *2023-01 - present*

- Kubernetes infrastructure administration
- Infrastructure as code
- Linux server administration

> Supermetrics is a product designed and developed for marketing teams that streamlines the delivery of data from 90+
> sales and marketing platforms into the reporting, analytics, and data storage tools that marketers already know.

My role has mainly focused on setting up and administrating Kubernetes infrastructures for different use cases such as
stateful databases, multitenant development environments, providing internal tooling, and the to host the main SaaS
application. I've been teaching developers about Kubernetes and helping them implement their workloads so that they can
be deployed into Kubernetes and can take advantage of for example the autoscaling and self healing aspects of
Kubernetes.

In addition to tasks around Kubernetes I've also been working with other public cloud services around different aspects
of the company such as managed databases, message queueing systems, object storages, IAM, artifact storage, and secret
management. Also some of my tasks have been around helping with CI/CD pipelines, creating reusable IaC modules, and
setting up self-serve projects based on IaC.

### Senior DevOps Engineer at [Nixu](https://www.nixu.com/) *2020-12 - 2022-12*

- Continuous integration & delivery pipeline development
- Infrastructure as code
- Linux server administration
- Internal services development and administration

> Nixu is a cybersecurity services company on a mission to keep the digital society running. We help organizations
> embrace digitalization securely.

It has been mostly on me to keep implement and improve CI/CD pipelines with GitLab CI/CD, and and implement IaC modules
and structure in Terraform for multiple clouds and organizations. In addition to that I've created internal tooling
with OCI images, Python, shell scripting, and combining these with existing cli-tools such as vault, aws, azure, vcd
etc. These are of course on top of regular Linux server administration tasks and service development.

### System Developer at [Utopia Analytics](https://utopiaanalytics.com/) *2017-09 - 2020-12*

- Linux server administration
- SaaS software development

Utopia Analytics is a text analytics company with their main product being [Utopia AI Moderator](https://utopiaanalytics.com/utopia-ai-moderator/).

> Utopia AI Moderator is a fully automated real-time moderation tool that
> protects your online community and your brand from abusive
> user-generated content, fraud, cyberbullies and spam.

My responsibility has been the application behind the API, all server and network infrastructure behind the company,
developing automation, all tech in our office etc.

### IT Specialist at [City of Helsinki](https://www.hel.fi/helsinki/en) *2009-11 - 2017-09*

- Linux server administration
- Windows server administration
- Network administration
- Windows client administration (via imaging/cloning)
- Software development
- Game integrations

Former youth department of the City of Helsinki had a branch devoted to gaming. My work was to keep all of the ~150 PC's
and dozens of gaming consoles working. Most of my time was spent on developing an automation that would guide the users
to a free computer, limit their gaming time if there were more people than there is computers, and provide tools for
youth workers to handle the day-to-day activities needed in their work.

### Software Designer at [G-Works](https://g.works/) *2011-09 - 2012-12*

- Web development (mostly but not limited to backend development)

In this digital advertising agency we mainly did [WordPress](https://wordpress.org/) websites and
[Laravel](https://laravel.com/) web applications. More often than not even the WordPress websites would contain lots
of application functionality, like event and participation management, reporting tools etc.

### Other non-IT jobs

- Mover at Helppomuutto *2008-08 - 2010-03*
- Cashier at [Opteam](https://opteam.fi) *2005-09 - 2009-12*

## Positions of responsibility

### [The Group ry](https://web.archive.org/web/20200926145441/http://thegroup.fi/) - *2004 - 2018*

- President of the board *2009-08 - 2010-08*
- Vice president of the board *2008-08 - 2009-08*
- Head organizer in multiple LAN-events and gaming tournaments
- Team leader in multiple LAN-events and gaming tournaments

The Group was an organization that mainly arranged computer and gaming events throughout Helsinki metropolitan area. The
organization also had a project funded by the [Ministry of Education and Culture](https://minedu.fi/en/frontpage). The
main result of the project was the birth of [The Finnish Esports Federation](https://seul.fi/in-english/). During my
term as the president of the board I acted as the supervisor of the project.

## Education

- [Haaga-Helia University of Applied Sciences](https://www.haaga-helia.fi/en/frontpage) *2011 - 2013*
  Bachelor of Business Administration (BBA) - SMEs IT-expert
- [Aalto University](https://www.aalto.fi/en) *2008 - 2011* (incomplete)
  Computer Science
- [Reserve Officer School](https://en.wikipedia.org/wiki/Reserve_Officer_School_(Finland)) *2007 - 2008*
  Military and Strategic Leadership
- [Koillis-Helsingin lukio](https://www.hel.fi/melu/fi/helsingin-medialukio) *2003 - 2007*
  Matriculation certificate

## Skills

### Languages

- Finnish (mother tongue)
- English (fluent)
- Swedish (limited)

### Tech

I feel that it is really hard to list tech stuff I can handle. There is so much specifics, like languages, frameworks,
libraries and tools, but on the other hand there are some vague stuff like security, and DevOps, and then big picture
stuff like version control, GNU/Linux OS. What should I list, and what should I skip? How should I categorize these?

Well, here's a vague list of stuff separated by how well I think I know them and combined in loose categories.

- Universal tech stuff that I feel I'm pretty fluent at
  - Programming languages: Python (and Flask, Click, pip), (Ba)sh scripting, PHP (and Laravel, composer), HTML5
  - DevOps-ish tools: Puppet, Ansible, Salt, Terraform, GitLab CI/CD, GitHub Actions, ArgoCD
  - Logs and monitoring: Elasticsearch/OpenSearch, Logstash, InfluxDB, Grafana
  - Databases: PostgreSQL, SQLite, MySQL/MariaDB, Redis
  - Networking: Tinc, Wireguard
  - Other tools: Borg Backup, Git, GitLab, Github, Hashicorp Vault
  - Containers: Docker/OCI containers, Kubernetes
  - Operating systems: Linux (Ubuntu, Debian, Rocky Linux)
  - Clouds: AWS, GCP, Hetzner, OVH
  - File formats: JSON (and `jq` stuff), YAML, Markdown (and `mermaid` stuff)

- And some that I feel like I know ok-ish
  - Programming languages: Javascript, CSS, Ruby
  - Events/Messaging: Kafka, RabbitMQ, PubSub
  - Clouds: Azure, AzureAD, Scaleway, VMWare vSphere/NSX-V/NSX-T/VCD
  - Other tools: Nextcloud, Unifi
  - Operating systems: Windows

- And finally some that I have touched more than once but wouldn't say I know much to brag about
  - Programming languages: Java, C, C#, C++, JS/Vue
  - Networking: Cisco, MikroTik
  - Databases: MongoDB
  - Operating systems: MacOS
  - Other tools: Airflow, Luigi, OpenLDAP

## Source

Source of this CV is at [https://gitlab.com/Zachu/cv](https://gitlab.com/Zachu/cv).
